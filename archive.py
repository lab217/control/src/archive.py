import collections
import statistics
from pylab import *


def G(a, x):  # Функция из первой лабораторной работы первого семестра
    func = (6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)) / (9 * a ** 2 + 30 * a * x + 16 * x ** 2)
    return func


def rectangle(g, a, xmin, xmax, intervals=8):
    dx = (xmax - xmin) / intervals
    area = 0
    x = xmin
    for i in range(intervals):
        area += dx * g(a, x)
        x += dx
    return area


# def rectangle_method(g, a, lower, upper, intervals=8): #Метод прямоугольников
# #Схож с дискретизацией сигнала, выглядит аналогично
# area = 0
# x = upper - lower
# for r in range(len(np.linspace(lower, upper, intervals))): #Цикл по длине(кол-ву) прямоугольников
#     area += g(a, lower + r * x / intervals) * x / intervals #Вычисление функции g по интервалу
# return area


def simpson(f, a, lower, upper, intervals=8):  # Метод Симпсона
    if intervals % 2 == 0 and intervals >= 0:  # Определение чётности интервала и то, что он больше 0
        h = (upper - lower) / intervals  # Шаг интервала
        z = intervals / 2  # Разбиение интервала
        temp = 0
        x = lower + h  #
        for i in range(1, int(z + 1)):  # От i до половины интервала(но +1)
            res = f(a, x)  # Вычисление функции
            temp += 4 * res  # Передача значения функции, умноженной на 4 шага
            x += 2 * h
        x = lower + 2 * h
        for i in range(1, int(z)):  # От i до половины интервала(z)
            res = f(a, x)
            temp += 2 * res
            x += 2 * h
        res_low = f(a, lower)
        res_up = f(a, upper)
        ans = (h / 3) * (res_up + res_low + temp)  # Вычисление метода Симпсона
        return ans
    else:
        return None


def trapezoid(f, a, lower, upper, intervals=8):  # Метод трапеций
    # Схож с методом прямоугольников, но верхняя линия под углом(трапеция)
    h = (upper - lower) / intervals  # Вычисление количество шагов по интервалу
    integration = f(a, lower) + f(a, upper)  # Нахождение суммы
    for i in range(1, intervals):  # Цикл от 1 до интервала по нахождению количества трапеций
        k = lower + i * h
        integration = integration + 2 * f(a, k)
    integration = integration * h / 2  # Общая сумма трапеций
    return integration


def sma_filter(data, window=3):  # Простая средняя - просто фильтр
    lst = data
    queue = collections.deque()  # Создание очереди
    for i in range(len(lst)):  # Цикл по длине массива для передачи данных в лист
        queue.append(lst[i])
        queue_length = len(queue)
        if queue_length > window:  # пока длина массива больше скользящей, константы
            queue.popleft()  # удаление и возвращение первого элемента очереди
            queue_length -= 1
        if queue_length == 0:  # если длина очереди 0, то значение индекса листа тоже 0
            lst[i] = 0
        else:  # иначе индекс листа равен разности суммы проверенных значений на длину очереди
            lst[i] = sum(queue) / queue_length
    return lst


# Скользящая средняя
def ema_filter(data, window=3):
    lst = data
    queue = collections.deque()
    for i in range(len(lst)):
        queue_length = len(queue)
        if queue_length == 0:
            queue.append(lst[i])
            pass
        elif queue_length > window:
            queue.popleft()
        alpha = 2 / (window + 1)
        lst[i] = (alpha * lst[i]) + ((1 - alpha) * queue[-1])
        queue.append(lst[i])
    return lst


# Медианный фильтр
def smm_filter(data, window=3):
    lst = data
    queue = collections.deque()
    for i in range(len(lst)):
        queue.append(lst[i])
        queue_length = len(queue)
        if queue_length > window:
            queue.popleft()
            queue_length -= 1
        if queue_length == 0:
            median = 0
        else:
            median = statistics.median(queue)
            lst[i] = median
    return lst
